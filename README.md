# README #

A Jupiter notebook for installing GoLang in a Google-Colab server environment and running an example Hello-World application within it.

### What is this repository for? ###

Enables running GoLang code freely within Google-Colab remote servers.

### How do I get set up? ###

Open Google-Colab and upload the file InstallGoLangAndRunAGeneratedHelloWorldApplication.ipynb

Run the code snippets inside of it in the order in which they appear from top to bottom and after you will have GoLang installed on your 
Google-Colab remote server with an example GoLang Hello-World application up and running, And you will be ready to go and build and run your 
own GoLang applications freely on Google-Colab remote servers.
